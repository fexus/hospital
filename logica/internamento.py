from logica import paciente
from logica import medico

internamentos = []
_codigo_inter = 0

def _gerar_codigo():
    _codigo_inter += 1
    return _codigo_inter

def internar_paciente(cpf_paciente, crm_medico, numero_quarto):
    codigo = _gerar_codigo()

    p = paciente.buscar_paciente(cpf_paciente)
    m = medico.buscar_medico(crm_medico)

    inter = [codigo, p, m, numero_quarto]
    
def listar_internamentos():
    return internamentos

def buscar_internamento(cod_internamento):
    for i in internamentos:
        if (i[0] == cod_internamento):
            return i
    return None

def cancelar_internamento(cod_internamento):
    for i in internamentos:
        if (i[0] == cod_internamento):
            internamentos.remove(i)
            return True
    return False

def iniciar_internamentos():
    internar_paciente(22222222222, 22222222222, 109)
    internar_paciente(11111111111, 11111111111, 203)
    internar_paciente(22222222222, 11111111111, 708)
