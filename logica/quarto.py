quartos = []
codigo_geral = 0

def _gerar_codigo():
    codigo_geral += 1
    return codigo_geral

def adicionar_quarto(numero):
    codigo = _gerar_codigo()
    
    quartos.append(codigo, numero)
    
def listar_quartos():
    return quartos

def buscar_quarto(codigo):
    for q in quartos:
        if(q[0] == codigo):
            return q
    return None

def remover_medico(codigo):
    for q in quartos:
        if(q[0] == codigo):
            quartos.remove(q)
            return True
    return False

def iniciar_quartos():
    adicionar_quarto(109)
    adicionar_quarto(203)
    adicionar_quarto(708)
